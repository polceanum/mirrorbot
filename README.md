# README #

This repository contains the sources for MirrorBot, one of the winners of the 2012 2K BotPrize competition and first place entry in BotPrize 2014.

### Getting it to work ###

* Install Netbeans, Pogamut, etc.
Tutorial: https://www.youtube.com/watch?v=KALrwgtwzao

### License ###

* MirrorBot is released to the community under the permissive MIT license.

### References ###

Polceanu, M. "Mirrorbot: Using human-inspired mirroring behavior to pass a turing test." IEEE Conference on Computational Intelligence in Games (CIG), 2013, pp. 1-8.

Polceanu, M., Garc�a, A. M., Jim�nez, J. L., Buche, C., & Leiva, A. J. F. "The Believability Gene in Virtual Bots". In The Twenty-Ninth International Flairs Conference, 2016, pp. 346-349.
